<html>
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <title>Hotel-Bookings</title>
    </head>
    <body>
        <div class="container">
            <h1 class="text-center mt-4">Hotel Bookings</h1>
            <h3 class="text-center">Main statistics</h3>
            <hr>
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <h3>The unluckiest customers</h3>
                    <table class="table align-middle">
                        <thead>
                            <tr>
                                <th scope="col">Customer ID</th>
                                <th scope="col">Number of rejected orders</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($unluckyCustomers as $customer)
                                <tr>
                                    <td>{{ $customer->customer_id }}</td>
                                    <td>{{ $customer->rejections  }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-6 col-md-12">
                    <h3>Weekly profit</h3>
                    <table class="table align-middle">
                        <thead>
                            <tr>
                                <th scope="col">Week</th>
                                <th scope="col">Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($weeklyProfit as $week)
                                <tr>
                                    <td>{{ $week->week }}</td>
                                    <td>{{ $week->profit  }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div class="col-lg-6 col-md-12">
                    <h3>Lowest weekend stays</h3>
                    <table class="table align-middle">
                        <thead>
                            <tr>
                                <th scope="col">Hotel ID</th>
                                <th scope="col">Number of total weekend stays</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($lowestWeekendStays as $weekendStays)
                                <tr>
                                    <td>{{ $weekendStays->hotel_id }}</td>
                                    <td>{{ $weekendStays->rented_amount }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>
