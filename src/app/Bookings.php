<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Bookings extends Model
{
    protected $guarded = [];

    public function getUnluckyCustomers()
    {
        return DB::table('bookings')
                  ->select(['customer_id', DB::raw('COUNT(*) as rejections')])
                  ->where('accepted', '=', '0')
                  ->groupBy('customer_id')
                  ->orderBy('rejections', 'desc')
                  ->orderBy('customer_id', 'asc')
                  ->limit(5)
                  ->get();
    }

    public function getWeeklyProfit()
    {
        return DB::table('bookings')
            ->select([DB::raw('WEEK(purchase_day, 7) AS week'), DB::raw('SUM(sales_price) - SUM(purchase_price) AS profit')])
            ->where('accepted', '=', '1')
            ->groupBy(DB::raw('WEEK(purchase_day, 7)'))
            ->get();
    }
}
