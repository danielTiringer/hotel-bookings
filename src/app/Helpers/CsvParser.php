<?php

namespace App\Helpers;

use Illuminate\Support\Facades\Storage;

class CsvParser
{
    public static function parse(string $filename): array
    {
        $csvData = Storage::disk('local')->get('public/' . $filename . '.csv');
        $dataLines = preg_split('/\s+/', $csvData);

        array_shift($dataLines);
        array_pop($dataLines);

        return $dataLines;
    }
}
