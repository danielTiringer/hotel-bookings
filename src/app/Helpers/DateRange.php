<?php

namespace App\Helpers;

use DateInterval;
use DatePeriod;
use DateTime;

class DateRange
{
    public static function createArray(string $arrivalDate, int $nights)
    {
        if ($nights == 1) {
            return [$arrivalDate];
        }

        $start = new DateTime($arrivalDate);
        $interval = new DateInterval('P1D');
        $endString = '+' . ($nights - 1) . ' day';
        $end = (new DateTime($arrivalDate))->modify($endString);
        $dateRange = new DatePeriod($start, $interval, $end);

        $dates = [];
        foreach ($dateRange as $date) {
            $dates[] = $date->format('Y-m-d');

        }
        return $dates;
    }
}
