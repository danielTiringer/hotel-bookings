<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Capacity extends Model
{
    protected $guarded = [];

    public function getLowestWeekendStays()
    {
        return DB::table('capacities')
                  ->select(['hotel_id', DB::raw('SUM(capacity_initial) - SUM(capacity_current) AS rented_amount')])
                  ->whereRaw('WEEKDAY(date) IN(4, 5)')
                  ->groupBy('hotel_id')
                  ->orderBy('rented_amount', 'asc')
                  ->limit(5)
                  ->get();
    }
}
