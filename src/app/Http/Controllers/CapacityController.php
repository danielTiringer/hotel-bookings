<?php

namespace App\Http\Controllers;

use App\Capacity;
use App\Helpers\CsvParser;
use Illuminate\Http\Request;

class CapacityController extends Controller
{
    public function fillDatabase()
    {
        $csvData = CsvParser::parse('capacity');
        $formattedData = $this->formatData($csvData);

        $capacity = Capacity::all();

        if ($capacity->isEmpty()) {
            foreach ($formattedData as $data) {
                Capacity::updateOrCreate(
                   [
                       'hotel_id' => $data['hotel_id'],
                       'date' => $data['date'],
                   ],
                   [
                       'capacity_initial' => $data['capacity_initial'],
                       'capacity_current' => $data['capacity_current'],
                   ],
                );
            }
        }
    }

    private function formatData(array $dataLines): array
    {
        $data = array_map(function($line) {
            $lineArray = explode(',', $line);
            return [
                'hotel_id' => $lineArray[0],
                'date' => $lineArray[1],
                'capacity_initial' => $lineArray[2],
                'capacity_current' => $lineArray[2],
            ];
        }, $dataLines);

        return $data;
    }
}
