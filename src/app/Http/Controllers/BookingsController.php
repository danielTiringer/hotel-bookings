<?php

namespace App\Http\Controllers;

use App\Bookings;
use App\Capacity;
use App\Helpers\CsvParser;
use App\Helpers\DateRange;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class BookingsController extends Controller
{
    public function processBookings()
    {
        $bookings = Bookings::orderBy('purchase_day')
                  ->orderBy('id')
                  ->get();

        foreach ($bookings as $booking) {
            $this->processBooking($booking);
        }
    }

    private function processBooking(Bookings $booking)
    {
        $dateRange = DateRange::createArray($booking->arrival_date, $booking->nights);

        DB::beginTransaction();

        try {
            foreach ($dateRange as $date) {
                $capacity = Capacity::where([
                    'hotel_id' => $booking->hotel_id,
                    'date' => $date,
                ])->first();

                if ($capacity == null) {
                    throw new Exception('There is no data for hotel ' . $booking->hotel_id . ' on date: ' . $date . '. The booking id is ' . $booking->id);
                }

                if ($capacity['capacity_current'] == 0) {
                    throw new Exception('There is no capacity to take order ' . $booking->id . ' for hotel ' . $booking->hotel_id . ' on date: ' . $date);
                }

                if ($capacity['capacity_current'] > 0) {
                    $capacity->capacity_current--;
                    $capacity->save();
                }
            }

            $booking->accepted = true;
            $booking->save();
            DB::commit();
        } catch (Exception $e) {
            DB::rollBack();
            $booking->accepted = false;
            $booking->comment = $e->getMessage();
            $booking->save();
        }
    }

    public function fillDatabase()
    {
        $csvData = CsvParser::parse('bookings');
        $formattedData = $this->formatData($csvData);

        $bookings = Bookings::all();

        if ($bookings->isEmpty()) {
            foreach ($formattedData as $data) {
                $this->store($data);
            }
        }
    }

    private function store(array $data)
    {
        $booking = new Bookings();
        $booking->id = $data['id'];
        $booking->hotel_id = $data['hotel_id'];
        $booking->customer_id = $data['customer_id'];
        $booking->sales_price = $data['sales_price'];
        $booking->purchase_price = $data['purchase_price'];
        $booking->arrival_date = $data['arrival_date'];
        $booking->purchase_day = $data['purchase_day'];
        $booking->nights = $data['nights'];
        $booking->save();
    }

    private function formatData(array $dataLines): array
    {
        $data = array_map(function($line) {
            $lineArray = explode(',', $line);
            return [
                'id' => $lineArray[0],
                'hotel_id' => $lineArray[1],
                'customer_id' => $lineArray[2],
                'sales_price' => $lineArray[3],
                'purchase_price' => $lineArray[4],
                'arrival_date' => $lineArray[5],
                'purchase_day' => $lineArray[6],
                'nights' => $lineArray[7],
            ];
        }, $dataLines);

        return $data;
    }
}
