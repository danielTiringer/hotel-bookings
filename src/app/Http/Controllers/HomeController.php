<?php

namespace App\Http\Controllers;

use App\Bookings;
use App\Capacity;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        $bookings = new Bookings();
        $capacity = new Capacity();

        $data = [
            'unluckyCustomers' => $bookings->getUnluckyCustomers(),
            'weeklyProfit' => $bookings->getWeeklyProfit(),
            'lowestWeekendStays' => $capacity->getLowestWeekendStays(),
        ];

        return view('index', $data);
    }
}
