# Hotel-Bookings

In order to try this repository, clone it to your computer, then:

Copy the `env` files:
``` sh
cp env.example .env
cp src/.env.example src/.env
```
You will want to source the `.env` file, or re-open your terminal.

Run composer install, start the containers, then (give a minute for the db to start, and) run the migrations:
``` sh
docker-compose run --rm php composer install
docker-compose up -d
docker-compose run --rm artisan migrate
```

The `storage` folder might want more access:

``` sh
sudo chmod -R 777 src/storage
```

The data load into the database is url based - go to the following urls once, and wait there while it loads:
`http://localhost:8080/capacity`
`http://localhost:8080/bookings`
`http://localhost:8080/process`

Finally, go to the `http://localhost:8080/` url to see the statistics page
